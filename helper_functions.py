import time
import unittest

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException

from Configs.settings import base_url
from Configs.settings import gecko_path

from log import logger


class Page(unittest.TestCase):
    """
    Contains functions, applicable for all pages of the site
    Parent class for specific pages
    """

    @classmethod
    def setUpClass(cls):
        """
        Initialize Selenium driver for tests
        :return:
        """
        try:
            logger.info("Trying to set up Webdriver")
            cls.driver = webdriver.Firefox(executable_path=gecko_path)
            logger.info("Webdriver started.")
            # cls.driver.get(base_url)
        except:
            logger.exception("Exception starting Webdriver")

    def check_title(self, expected_title):
        self.driver.get(base_url)
        try:
            logger.info("check_title called.")
            assert expected_title in self.driver.title
        except AssertionError:
            logger.error("check_title: Wrong page title. Actual title: %s. Expected title: %s", self.driver.title,
                         expected_title)
        except:
            logger.exception("check_title")

    @classmethod
    def tearDownClass(cls):
        """
        Close Selenium Driver
        :return:
        """
        cls.driver.close()
        logger.info("Webdriver closed.")


class LoginPage(Page):
    """
    Проверяем наличие ссылки на логин
    """

    def check_login_link_present(self):
        try:
            login_link = self.driver.find_element_by_id('loginLink')
            logger.info(login_link)
            return login_link
        except NoSuchElementException:
            logger.error("No login link")
        except:
            logger.exception("Login link test error")

    """
     Провeряем наличие поля мэйла
     """

    def check_email_field_present(self):
        # TODO: code was taken from SO, could be rewritten
        # http://stackoverflow.com/questions/26566799/selenium-python-how-to-wait-until-the-page-is-loaded
        delay = 3  # seconds
        try:
            self.driver.implicitly_wait(3)
            logger.info('waited 3 seconds')
            email_field = self.driver.find_element_by_id("Email")
            return email_field

        except TimeoutException:
            logger.error("Loading took too much time!")
        except NoSuchElementException:
            logger.error("No email field")
        except:
            logger.exception("Email field test error")

    """
    Проверяем наличие поля пароля
    """

    def check_password_field_present(self):
        try:
            password_field = self.driver.find_element_by_id("Password")
            return password_field
        except NoSuchElementException:
            logger.error("No password field")
        except:
            logger.exception("Password function")

    """
    Проверяем наличие кнопки логина
    """

    def check_button_present(self):
        try:
            button = self.driver.find_element_by_css_selector("input.btn.btn-success")
            return button
        except NoSuchElementException:
            logger.error("No login button")
        except:
            logger.exception("Login button function")

    """
    Проверяем возможность логина
    """

    def check_login(self, email, password, name):
        wd = self.driver
        logger.info("check_login called")
        wd.get(base_url)
        login_link = self.check_login_link_present()
        login_link.click()

        email_field = self.check_email_field_present()
        email_field.clear()
        email_field.send_keys(email)
        password_field = self.check_password_field_present()
        password_field.clear()
        password_field.send_keys(password)
        button = self.check_button_present()
        button.click()
        time.sleep(5)
        try:
            assert name in wd.page_source
        except AssertionError:
            logger.error("Login failed")
        except:
            logger.exception("Login function")
