import os
import yaml
import logging
import logging.config

def setup_logging():
    """
    Setup logging configuration
    """
    path = r'Configs/log.config'
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=logging.INFO)


setup_logging()
logger = logging.getLogger(__name__)