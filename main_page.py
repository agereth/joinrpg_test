import helper_functions


class MainPage(helper_functions.Page):
    """
    Test class for Main Page http://joinrpg.ru/
    """
    def test_title(self):
        self.check_title("Dev.JoinRpg.RU")

    # @helper_functions.unittest.expectedFailure
    def test_title_wrong(self):
        self.check_title("Джойни офигенную игру")

if __name__ == "__main__":
    helper_functions.unittest.main()