from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

def check_title(driver, logfile):
    try:
        assert "JoinRpg.RU" in driver.title
    except AssertionError:
        logfile.write("WrongTitle")
	
def check_login_present(driver, logfile):
    try:
        login_link = driver.find_element_by_id('loginlink')
        return login_link
    except NoSuchElementException:
        logfile.write("No login link")
		
def check_login_link(driver, loginlink, logfile):
    try:
	    assert(loginlink.link == r'/account/login')
    except AssertionError:
        logfile.write("Wrong login link")
  
	    
gecko_path=r'C:\Drivers\geckodriver.exe'
driver = webdriver.Firefox(executable_path=gecko_path)
site_name="http://joinrpg.ru" 
driver.get(site_name)
logfile = open("logfile.txt", "w")

check_title(driver, logfile)
login_link = check_login_present(driver, logfile)
check_login_link(driver, login_link, logfile)
logfile(close)
driver.close()
