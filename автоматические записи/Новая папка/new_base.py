# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, logging
logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.DEBUG, filename = u'mylog.log')


class NewBase(unittest.TestCase, set_driver, base_url):
    def setUp(self):
        self.driver = set_driver
        self.base_url = base_url

    def test_new_base(self):
        driver = self.driver
        driver.find_element_by_id("loginLink").click()
        driver.find_element_by_id("Email").clear()
        driver.find_element_by_id("Email").send_keys("agereth@gmail.com")
        driver.find_element_by_id("Password").clear()
        driver.find_element_by_id("Password").send_keys("juice87")
        driver.find_element_by_css_selector("input.btn.btn-success").click()
        try:
            new_base = driver.find_element_by_link_text(u"Создать базу заявок")
            newbase.click()
            try:
                assert "Cоздание базы заявок" in driver.title
                try:
                    project_name =  driver.find_element_by_id("ProjectName")\
                    project_name.clear()
                    project_name.send_keys("test_base")
                    try:
                        button_create = driver.find_element_by_css_selector("input.btn.btn-default")\
                        button.click()
                        try:
                            assert 'Игра "test_base"' in driver.title
                        except AssertionError:
                            logging.error("Кажется, игра не была создана")
                    except NoSuchElementException:
                        logging.error("Нет кнопки создания игры")
                except NoSuchElementException:
                    logging.error("Нет поля ввода названия игры")
            except AssertionError:
                logging.error("Переход на страницу заявок не произошел")


    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
