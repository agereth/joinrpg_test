# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

driver = webdriver.Firefox()
gecko_path=r'C:\Drivers\geckodriver.exe'
driver = webdriver.Firefox(executable_path=gecko_path)
base_url = r'http://joinrpg.ru/'

class Login(unittest.TestCase):
    def setUp(self, driver, base_url):
        self.driver = driver
        self.base_url = base_url
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_login(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("loginLink").click()
        driver.find_element_by_id("Email").clear()
        driver.find_element_by_id("Email").send_keys("agereth@gmail.com")
        driver.find_element_by_id("Password").clear()
        driver.find_element_by_id("Password").send_keys("juice87")
        driver.find_element_by_css_selector("input.btn.btn-success").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
