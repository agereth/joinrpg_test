# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re, logging
logging.basicConfig(format = u'%(levelname)-8s [%(asctime)s] %(message)s', level = logging.DEBUG, filename = u'mylog.log')

class Forgottenpassword(unittest.TestCase, set_driver, base_url):
    def setUp(self):
        self.driver = set_driver
        self.base_url = base_url
    
    def test_forgottenpassword(self):
        driver = self.driver
        driver.get(self.base_url + "/account/login")
        try:
            element = driver.find_element_by_link_text(u"Забыли пароль?")
            element.click()
            try:
                assert "Забыли пароль" in driver.title
                try:
                    email = driver.find_element_by_id("Email")
                    email.clear()
                    email.send_keys("agereth@gmail.com")
                    try:
                        button = driver.find_element_by_css_selector("input.btn.btn-default")
                        button.click()
                        try:
                            assert "Восстановление пароля" in driver.title
                        except AssertionError:
                            logging.error("Something wrong with password reset")
                    except NoSuchElementException:
                        logging.error("No reset password link")
            except AssertionError:
                logging.Error("Wrong reset password page")
        except NoSuchElementException:
            logging.error("No reset password link")
    
    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
