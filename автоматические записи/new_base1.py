# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class NewBase1(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://joinrpg.ru/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_new_base1(self):
        driver = self.driver
        driver.get(self.base_url + "/")
        driver.find_element_by_id("loginLink").click()
        driver.find_element_by_id("Email").clear()
        driver.find_element_by_id("Email").send_keys("agereth@gmail.com")
        driver.find_element_by_id("Password").clear()
        driver.find_element_by_id("Password").send_keys("juice87")
        driver.find_element_by_css_selector("input.btn.btn-success").click()
        driver.find_element_by_link_text(u"Создать базу заявок").click()
        driver.find_element_by_id("ProjectName").clear()
        driver.find_element_by_id("ProjectName").send_keys("test_base")
        driver.find_element_by_css_selector("input.btn.btn-default").click()
        driver.find_element_by_link_text(u"Прочее").click()
        driver.find_element_by_link_text(u"Прочее").click()
        # ERROR: Caught exception [ERROR: Unsupported command [selectWindow | name=1015 | ]]
        driver.find_element_by_id("loginLink").click()
        driver.find_element_by_link_text(u"Забыли пароль?").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
