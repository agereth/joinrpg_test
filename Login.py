import helper_functions
from helper_functions import LoginPage
import unittest


class Login_Functions(LoginPage):
    """
        Test class for Login Functions http://joinrpg.ru/
    """

    def test_login(self):
        self.check_login("agereth@gmail.com", "juice87", "Юсеницо")


if __name__ == "__main__":
    # helper_functions.unittest.main()
    unittest.main()
